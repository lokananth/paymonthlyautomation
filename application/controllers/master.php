<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {	
			session_start();
		}
		//echo '<pre>';print_r($_SESSION);exit;
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	public function index()
	{
		if(isset($_REQUEST['masterDate']) && $_REQUEST['masterDate']!=''){		
		
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];
			$varDate = trim($_REQUEST['masterDate']);
			
			//Get call History Master 
			//$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode,'Call'=>'100','SMS'=>'100','GPRS'=>'500');
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode,'Call'=>'1','SMS'=>'1','GPRS'=>'1');
			//echo '<pre>';print_r($params);exit;
			$arrMasterCallHistoryRes = ApiPostHeader($this->config->item('GetCallMasterBalanceHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrMasterCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallMasterBalanceHistory');exit;
			
			if(isset($arrMasterCallHistoryRess['errcode']) && $arrMasterCallHistoryRes['errcode']=='-1'){			
				$data['arrMasterCallHistory'] = array();
			}
			else if(count($arrMasterCallHistoryRes)>0 && $arrMasterCallHistoryRes[0]['errcode']=='0'){			
				$data['arrMasterCallHistory'] = $arrMasterCallHistoryRes;		
			}else{
				$data['arrMasterCallHistory'] = array();
			}			
		}else{			
			
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];
			$varDate = @date('Y-m-d');
			
			//Get call History Master 
			//$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode,'Call'=>'100','SMS'=>'100','GPRS'=>'500');
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode,'Call'=>'1','SMS'=>'1','GPRS'=>'1');
			//echo '<pre>';print_r($params);exit;
			$arrMasterCallHistoryRes = ApiPostHeader($this->config->item('GetCallMasterBalanceHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrMasterCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallMasterBalanceHistory');exit;
			
			if(isset($arrMasterCallHistoryRess['errcode']) && $arrMasterCallHistoryRes['errcode']=='-1'){			
				$data['arrMasterCallHistory'] = array();		
			}else if(count($arrMasterCallHistoryRes)>0 && $arrMasterCallHistoryRes[0]['errcode']=='0'){			
				$data['arrMasterCallHistory'] = $arrMasterCallHistoryRes;		
			}else{
				$data['arrMasterCallHistory'] = array();
			}		
		}		
		
		$data['varProductCode'] = $varProductCode;
		$data['varSiteCode'] = $varSiteCode;
		$data['varDate'] = $varDate;
		
		$this->load->view('header_view');
		$this->load->view('innerHeader_view');		
		$this->load->view('leftMenu_view');
		$this->load->view('master_view',$data);
		$this->load->view('footer_view');
	}
	
	public function getMasterInfoByMobileNo(){		
		$varMobileNo =  $this->input->post('mobileNo');		
		
		//Get call History Master based upon mobile number
		$params = array('mobileno'=>$varMobileNo);
		$arrGetMasterByMobile = ApiPostHeader($this->config->item('GetCallMasterHistoryByMobileNo'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetMasterByMobile);print_r($_SESSION);echo $this->config->item('GetCallMasterHistoryByMobileNo');exit;
		if($arrGetMasterByMobile[0]['errcode']=='0'){
			echo json_encode($arrGetMasterByMobile[0]);exit;
		}else{
			echo '-1';exit;
		}		
	}
	
	public function blockMasterByMobileNo(){	
		
		if(isset($_REQUEST['mobileNo']) && $_REQUEST['mobileNo']!=''){			
				$varMobileNo =  trim($_REQUEST['mobileNo']);				
				$varReason = strip_tags(addslashes(trim($_REQUEST['bundleBlockReason'])));				
				
				//Get call History Bundles based upon mobile number
				$params = array('sitecode'=>$_SESSION['siteCode'],'usertype'=>'2','userinfo'=>$varMobileNo,'calledby'=>'UMP_Block','Mundio_product'=>$_SESSION['productCode'],'status'=>'1','Reason'=>$varReason,'userid'=>$_SESSION['userId']);
				$arrBlockByMobile = ApiPostHeader($this->config->item('BlockMobileSim'), $params);
				//echo '<pre>';print_r($params);print_r($arrBlockByMobile);print_r($_SESSION);echo $this->config->item('BlockMobileSim');exit;
				
				if($arrBlockByMobile[0]['errcode']=='0'){
					$this->session->set_flashdata('successMessage','Mobile Number Blocked Successfully !!!');
					redirect('master');
				}else{
					$this->session->set_flashdata('errorMessage',$arrBlockByMobile['errmsg']);
					redirect('master');
				}
		}else{
			$this->session->set_flashdata('errorMessage','Please check the mobile no');
			redirect('master');
		}				
	}
	
	public function downloadsMasterList($varDate='')
	{	
		if(isset($varDate) && $varDate!=''){		
		
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];			
			
			//Get call History Master 
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode,'Call'=>'100','SMS'=>'100','GPRS'=>'500');
			$arrMasterCallHistoryRes = ApiPostHeader($this->config->item('GetCallMasterBalanceHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrMasterCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallMasterBalanceHistory');exit;
			
			if(count($arrMasterCallHistoryRes)>0 && $arrMasterCallHistoryRes[0]['errcode']=='0'){			
				$arrMasterCallHistory = $arrMasterCallHistoryRes;		
			}else{
				$arrMasterCallHistory = array();
			}			
		}else{			
			
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];
			$varDate = @date('Y-m-d');
			
			//Get call History Master 
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode,'Call'=>'100','SMS'=>'100','GPRS'=>'500');
			$arrMasterCallHistoryRes = ApiPostHeader($this->config->item('GetCallMasterBalanceHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrMasterCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallMasterBalanceHistory');exit;
			
			if(count($arrMasterCallHistoryRes)>0 && $arrMasterCallHistoryRes[0]['errcode']=='0'){			
				$arrMasterCallHistory = $arrMasterCallHistoryRes;		
			}else{
				$arrMasterCallHistory = array();
			}
		}		
		
		$data['varProductCode'] = $varProductCode;
		$data['varSiteCode'] = $varSiteCode;
		$data['varDate'] = $varDate;
		
		//echo '<pre>';print_r($arrMasterCallHistory);exit;		
		
		date_default_timezone_set('Europe/London');
		$this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Master Balance Call, SMS & Data');

        for ($col = 'A'; $col != 'H'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:P1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Master Balance Call, SMS & Data '.$varDate);
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:G2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $this->excel->getActiveSheet()->getStyle("A2:G2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:G2')->getFont()->getColor()->setRGB('FFFFFF');;
		$this->excel->getActiveSheet()->getStyle('A3:G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$this->excel->getActiveSheet()->getStyle('A3:A256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('B3:B256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('C3:C256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('D3:D256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);		
		$this->excel->getActiveSheet()->getStyle('E3:E256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('F3:F256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('G3:G256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		//$this->excel->getActiveSheet()->getStyle('H3:H256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
						
        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'CLI')
                ->setCellValue('B2', 'Brand')
                ->setCellValue('C2', 'Customer Name')
				->setCellValue('D2', 'Calls')
				->setCellValue('E2', 'SMS')
				->setCellValue('F2', 'Data / (MB)')
				->setCellValue('G2', 'Previous History');
				//->setCellValue('H2', 'Previous History');				
				

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;		 
        
        foreach ($arrMasterCallHistory AS $item) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('A' . $i . '')->getNumberFormat()->setFormatCode('0000');
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
			$this->excel->getActiveSheet()->getStyle('F' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('G' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			//$this->excel->getActiveSheet()->getStyle('H' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $item['mobileno'])
                    ->setCellValue('B' . $i, $item['Brand'])
                    ->setCellValue('C' . $i, $item['fullname'])
					->setCellValue('D' . $i, $item['MasterCall'])
					->setCellValue('E' . $i, $item['MasterSMS'])
					->setCellValue('F' . $i, $item['MasterGPRS'])
                    ->setCellValue('G' . $i, $item['Prehistory']);
					//->setCellValue('H' . $i, $item['Prehistory']);				
					
            $i++;
        }

        $filename = ' Master Balance Call, SMS & Data - '.$varDate.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
		exit;		
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */