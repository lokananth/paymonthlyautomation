<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statusoverview extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {	
			session_start();
		}
		//echo '<pre>';print_r($_SESSION);exit;
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	public function index()
	{		
		if(isset($_REQUEST['bundleDate']) && $_REQUEST['bundleDate']!=''){		
		
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];
			$varDate = trim($_REQUEST['bundleDate']);
			
			//Get call History Bundles
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode);
			//echo '<pre>';print_r($params);exit;
			$arrBundleCallHistoryRes = ApiPostHeader($this->config->item('GetCallBundlesHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrBundleCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallBundlesHistory');exit;
			
			if(isset($arrMasterCallHistoryRess['errcode']) && $arrBundleCallHistoryRes['errcode']=='-1'){			
				$data['arrBundleCallHistory'] = array();		
			}else if(count($arrBundleCallHistoryRes)>0 && $arrBundleCallHistoryRes[0]['errcode']=='0'){			
				$data['arrBundleCallHistory'] = $arrBundleCallHistoryRes;		
			}else{
				$data['arrBundleCallHistory'] = array();
			}			
		}else{			
			
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];
			$varDate = @date('Y-m-d');
			
			//Get call History Bundles
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode);
			//echo '<pre>';print_r($params);exit;
			$arrBundleCallHistoryRes = ApiPostHeader($this->config->item('GetCallBundlesHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrBundleCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallBundlesHistory');exit;
			
			if(isset($arrMasterCallHistoryRess['errcode']) && $arrBundleCallHistoryRes['errcode']=='-1'){			
				$data['arrBundleCallHistory'] = array();		
			}else if(count($arrBundleCallHistoryRes)>0 && $arrBundleCallHistoryRes[0]['errcode']=='0'){			
				$data['arrBundleCallHistory'] = $arrBundleCallHistoryRes;		
			}else{
				$data['arrBundleCallHistory'] = array();
			}
		}		
		
		$data['varProductCode'] = $varProductCode;
		$data['varSiteCode'] = $varSiteCode;
		$data['varDate'] = $varDate;
		
		$this->load->view('header_view');
		$this->load->view('innerHeader_view');		
		$this->load->view('leftMenu_view');
		$this->load->view('bundle_view',$data);
		$this->load->view('footer_view');
	}
	
	public function searchByProductSiteCode($varProductCode='',$varSiteCode=''){
		//echo '<pre>';echo $varProductCode.' '.$varSiteCode;exit;				
		$varDate = @date('Y-m-d');		
		
		//Get call History Bundles
		$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode);
		//echo '<pre>';print_r($params);exit;
		$arrBundleCallHistoryRes = ApiPostHeader($this->config->item('GetCallBundlesHistory'), $params);
		//echo '<pre>';print_r($params);print_r($arrBundleCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallBundlesHistory');exit;
		
		if(isset($arrMasterCallHistoryRess['errcode']) && $arrBundleCallHistoryRes['errcode']=='-1' && isset($arrBundleCallHistoryRes['errcode'])){			
			$data['arrBundleCallHistory'] = array();		
		}else if(count($arrBundleCallHistoryRes)>0 && $arrBundleCallHistoryRes[0]['errcode']=='0' && isset($arrBundleCallHistoryRes[0]['errcode'])){			
			$data['arrBundleCallHistory'] = $arrBundleCallHistoryRes;		
		}else{
			$data['arrBundleCallHistory'] = array();
		}	
		
		$data['varProductCode'] = $varProductCode;
		$data['varSiteCode'] = $varSiteCode;
		$data['varDate'] = $varDate;
		
		$_SESSION['productCode'] = $varProductCode;
		$_SESSION['siteCode'] = $varSiteCode;
		
		$this->load->view('header_view');
		$this->load->view('innerHeader_view');		
		$this->load->view('leftMenu_view');
		$this->load->view('bundle_view',$data);
		$this->load->view('footer_view');		
	}

	public function getBundleInfoByMobileNo(){		
		$varMobileNo =  $this->input->post('mobileNo');
		$varCustCode =  $this->input->post('custCode');
		$varBundleId =  $this->input->post('bundleId');
		
		//Get call History Bundles based upon mobile number
		$params = array('mobileno'=>$varMobileNo,'custcode'=>$varCustCode,'bundleid'=>$varBundleId);
		$arrGetBundlesByMobile = ApiPostHeader($this->config->item('GetCallBundlesHistoryByMobileNo'), $params);
		//echo '<pre>';print_r($params);print_r($arrGetBundlesByMobile);print_r($_SESSION);echo $this->config->item('GetCallBundlesHistoryByMobileNo');exit;
		if($arrGetBundlesByMobile[0]['errcode']=='0'){
			echo json_encode($arrGetBundlesByMobile[0]);exit;
		}else{
			echo '-1';exit;
		}		
	}
	
	public function blockBundleByMobileNo(){	
		
		if(isset($_REQUEST['mobileNo']) && $_REQUEST['mobileNo']!=''){			
				$varMobileNo =  trim($_REQUEST['mobileNo']);				
				$varReason = strip_tags(addslashes(trim($_REQUEST['bundleBlockReason'])));				
				
				//Get call History Bundles based upon mobile number
				$params = array('sitecode'=>$_SESSION['siteCode'],'usertype'=>'2','userinfo'=>$varMobileNo,'calledby'=>'UMP_Block','Mundio_product'=>$_SESSION['productCode'],'status'=>'1','Reason'=>$varReason,'userid'=>$_SESSION['userId']);
				$arrBlockByMobile = ApiPostHeader($this->config->item('BlockMobileSim'), $params);
				//echo '<pre>';print_r($params);print_r($arrBlockByMobile);print_r($_SESSION);echo $this->config->item('BlockMobileSim');exit;
				
				if($arrBlockByMobile[0]['errcode']=='0'){
					$this->session->set_flashdata('successMessage','Mobile Number Blocked Successfully !!!');
					redirect('bundle');
				}else{
					$this->session->set_flashdata('errorMessage',$arrBlockByMobile['errmsg']);
					redirect('bundle');
				}
		}else{
			$this->session->set_flashdata('errorMessage','Please check the mobile no');
			redirect('bundle');
		}				
	}

	public function downloadsBundleList($varDate='')
	{	
		if(isset($varDate) && $varDate!=''){		
		
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];			
			
			//Get call History Bundles
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode);
			$arrBundleCallHistoryRes = ApiPostHeader($this->config->item('GetCallBundlesHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrBundleCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallBundlesHistory');exit;
			
			if(count($arrBundleCallHistoryRes)>0 && $arrBundleCallHistoryRes[0]['errcode']=='0'){			
				$arrBundleCallHistory = $arrBundleCallHistoryRes;		
			}else{
				$arrBundleCallHistory = array();
			}			
		}else{			
			
			$varProductCode = $_SESSION['productCode'];
			$varSiteCode = $_SESSION['siteCode'];
			$varDate = @date('Y-m-d');
			
			//Get call History Bundles
			$params = array('DateFrom'=>$varDate,'DateTo'=>$varDate,'mundio_product'=>$varProductCode,'sitecode'=>$varSiteCode);
			$arrBundleCallHistoryRes = ApiPostHeader($this->config->item('GetCallBundlesHistory'), $params);
			//echo '<pre>';print_r($params);print_r($arrBundleCallHistoryRes);print_r($_SESSION);echo $this->config->item('GetCallBundlesHistory');exit;
			
			if(count($arrBundleCallHistoryRes)>0 && $arrBundleCallHistoryRes[0]['errcode']=='0'){			
				$arrBundleCallHistory = $arrBundleCallHistoryRes;		
			}else{
				$arrBundleCallHistory = array();
			}
		}		
		
		$data['varProductCode'] = $varProductCode;
		$data['varSiteCode'] = $varSiteCode;
		$data['varDate'] = $varDate;
		
		//echo '<pre>';print_r($arrBundleCallHistory);exit;		
		
		date_default_timezone_set('Europe/London');
		$this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Bundles');

        for ($col = 'A'; $col != 'I'; $col++) {
            $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:P1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Bundles View Number List '.$varDate);
        $this->excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFill()->getStartColor()->setARGB('58C849');
        $this->excel->getActiveSheet()->getStyle("A2:H2")->getAlignment()->setVertical(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        $this->excel->getActiveSheet()->getStyle("A2:H2")->getFont()->setBold(true);
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->excel->getActiveSheet()->getStyle('A2:H2')->getFont()->getColor()->setRGB('FFFFFF');;
		$this->excel->getActiveSheet()->getStyle('A3:H3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
		$this->excel->getActiveSheet()->getStyle('A3:A256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('B3:B256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('C3:C256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('D3:D256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);		
		$this->excel->getActiveSheet()->getStyle('E3:E256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('F3:F256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('G3:G256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		$this->excel->getActiveSheet()->getStyle('H3:H256')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
		
						
        $this->excel->setActiveSheetIndex(0)
                ->setCellValue('A2', 'CLI')
                ->setCellValue('B2', 'Brand')
                ->setCellValue('C2', 'Customer Name')
				->setCellValue('D2', 'Bundle Name')
				->setCellValue('E2', 'Bundle usage(Calls)')
				->setCellValue('F2', 'Bundle usage(SMS)')
				->setCellValue('G2', 'Bundle usage(Data) mb')
				->setCellValue('H2', 'Previous History');				
				

        $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
        $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
        

        $i = 3;		 
        
        foreach ($arrBundleCallHistory AS $item) {
            $this->excel->getActiveSheet()->getStyle('A' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('A' . $i . '')->getNumberFormat()->setFormatCode('0000');
            $this->excel->getActiveSheet()->getStyle('B' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('C' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('D' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('E' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);			
			$this->excel->getActiveSheet()->getStyle('F' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->excel->getActiveSheet()->getStyle('G' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->excel->getActiveSheet()->getStyle('H' . $i . '')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			
            $this->excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $item['mobileno'])
                    ->setCellValue('B' . $i, $item['Brand'])
                    ->setCellValue('C' . $i, $item['fullname'])
					->setCellValue('D' . $i, $item['bundlename'])
					->setCellValue('E' . $i, $item['BundleCall'])
					->setCellValue('F' . $i, $item['BunldeSMS'])
                    ->setCellValue('G' . $i, $item['BundleGPRS'])
					->setCellValue('H' . $i, $item['Prehistory']);				
					
            $i++;
        }

        $filename = ' Bundles View Number List - '.$varDate.'.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
		ob_end_clean();
		ob_start();
        $objWriter->save('php://output');
		exit;		
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */